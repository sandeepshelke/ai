'''
Part 3 - Classification
'''
import mlutil as mu
import matplotlib.pyplot as plt
import numpy as np

def show_plot(classifier,x,y,t,clrs=('red','green')):
    from matplotlib.colors import ListedColormap
    x1,x2 = np.meshgrid(
        np.arange(start=x[:, 0].min()-1, stop=x[:, 0].max()+1, step=0.01),
        np.arange(start=x[:, 1].min()-1, stop=x[:, 1].max()+1, step=0.01)
    )
    z = classifier.predict(np.array([x1.ravel(),x2.ravel()]).T)
    clrs = ListedColormap(clrs)
    plt.contourf(x1, x2, z.reshape(x1.shape), alpha=0.75, cmap=clrs)
    plt.xlim(x1.min(), x1.max())
    plt.xlim(x2.min(), x2.max())
    for i,j in enumerate(np.unique(y)):
        plt.scatter(x[y==j,0], x[y==j,1], c=clrs(i), label=j)
    plt.title(t)
    plt.xlabel('x')
    plt.ylabel('y')
    plt.legend()
    plt.show()

def PlotTrainTestSet(classifier, x_train, x_test, y_train,
                     y_test, y_pred, t, clrs=('red','green')):
    # Visualize training results
    show_plot(classifier, x_train, y_train, t+' (Training set)', clrs)
    # Create confusion matrix
    from sklearn.metrics import confusion_matrix
    cm = confusion_matrix(y_true=y_test,y_pred=y_pred)
    print(cm)
    # Visualize test results
    show_plot(classifier, x_test, y_test, t+' (Test set)', clrs)
    return None

def logistic_classifier(x, y, **kwargs):
    from sklearn.linear_model import LogisticRegression
    classifier = LogisticRegression(random_state=0)
    classifier.fit(x, y)
    return classifier

def knn_classifier(x, y, **kwargs):
    from sklearn.neighbors import KNeighborsClassifier
    classifier = KNeighborsClassifier(
        n_neighbors=5, metric='minkowski', p=2)
    classifier.fit(x, y)
    return classifier

def svm_classifier(x, y, **kwargs):
    from sklearn.svm import SVC
    classifier = SVC(kernel=kwargs['kernel'], random_state=0)
    classifier.fit(x, y)
    return classifier

def naive_bayes_classifier(x, y, **kwargs):
    from sklearn.naive_bayes import GaussianNB
    classifier = GaussianNB()
    classifier.fit(x, y)
    return classifier

def decision_tree_classifier(x, y, **kwargs):
    from sklearn.tree import DecisionTreeClassifier
    classifier = DecisionTreeClassifier(criterion='entropy',
                                        random_state=0)
    classifier.fit(x, y)
    return classifier

def random_forest_classifier(x, y, **kwargs):
    from sklearn.ensemble import RandomForestClassifier
    classifier = RandomForestClassifier(
        n_estimators=kwargs['trees'],
        criterion='entropy')
    classifier.fit(x, y)
    return classifier

# also known as conditional exponential classifier
class MaxEntropyClassifier(object):
    def __init__(self, x, y, **kwargs):
        self.features = f = kwargs['features']
        train_set = [(dict(zip(f, xi)), y[i]) for i,xi in enumerate(x)]
        from nltk.classify.maxent import MaxentClassifier
        self.mxec = MaxentClassifier.train(train_set, trace=0)

    def predict(self, test):
        if self.mxec is None:
            raise RuntimeError('Train the classifier first')
        pred = []
        for x in test:
            ts = dict(zip(self.features, x))
            pred.append(self.mxec.classify(ts))
        return pred

# Feature extraction using Principal Component Analysis 
def principal_comp_analysis(x_train, x_test, y_train, n_comp=2):
    from sklearn.decomposition import PCA
    pca = PCA(n_components=n_comp)
    x_train = pca.fit_transform(x_train)
    x_test = pca.transform(x_test)

    # Apply logistic regression classifier
    return (x_test, logistic_classifier(x_train, y_train))

# Feature extraction using Linear Discriminant Analysis 
def linear_discriminant_analysis(x_train, x_test, y_train, n_comp=2):
    from sklearn.discriminant_analysis import LinearDiscriminantAnalysis
    lda = LinearDiscriminantAnalysis(n_components=n_comp)
    x_train = lda.fit_transform(x_train, y_train)
    x_test = lda.transform(x_test)

    # Apply logistic regression classifier
    return (x_test, logistic_classifier(x_train, y_train))

# Feature extraction using Kernel Principal Component Analysis 
def kernel_PCA(x_train, x_test, y_train, n_comp=2):
    from sklearn.decomposition import KernelPCA
    kpca = KernelPCA(n_components=n_comp, kernel='rbf')
    x_train = kpca.fit_transform(x_train)
    x_test = kpca.transform(x_test)

    # Apply logistic regression classifier
    return (x_test, logistic_classifier(x_train, y_train))

