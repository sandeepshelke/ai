# -*- coding: utf-8 -*-
import numpy as np
import pandas as pd

# import data set
def import_dataset(fname,xs=0,xe=-1,ys=-1):
    dataset = pd.read_csv(fname)
    # read all rows, for all but last column
    x = dataset.iloc[:, xs:xe].values
    # read all rows, for last column
    y = dataset.iloc[:, ys].values
    return x,y

# handling missing data
def handle_missing(x):
    from sklearn.preprocessing import Imputer
    imputer = Imputer(missing_values='NaN',
                      strategy='mean',
                      axis=0)
    # fit only columns with missing data
    # that is starting from column 1 to 3
    # this can be identified by checking for NaN
    imputer = imputer.fit(x[:, 1:3])
    x[:, 1:3] = imputer.transform(x[:, 1:3])
    return x


# encoding categories
def encode_categories(x,y,indices=[-1],hot_encode=None):
    from sklearn.preprocessing import (LabelEncoder,
                                       OneHotEncoder)
    for i in indices:
        label_encoder_x = LabelEncoder()
        x[:,i] = label_encoder_x.fit_transform(x[:,i])

    if hot_encode is None:
        hot_encode = indices

    onehotencoder = OneHotEncoder(categorical_features=hot_encode)
    x = onehotencoder.fit_transform(x).toarray()

    if y:
        label_encoder_y = LabelEncoder()
        y = label_encoder_y.fit_transform(y)
    return x,y


# splitting the dataset in training and test set
def split(x, y, test_size=0.2):
    from sklearn.model_selection import train_test_split
    x_train, x_test, y_train, y_test = train_test_split(
        x, y, test_size=test_size, random_state=0)
    return (x_train,x_test),(y_train,y_test)

# feature scaling
# standardisation and normalisation
def feature_scale(train, test=None):
    from sklearn.preprocessing import StandardScaler
    sc = StandardScaler()
    train = sc.fit_transform(train)
    if test.any():
        test = sc.transform(test)
    return train,test

def acc_prec_recall_f1(cm):
    '''
    True positive: prediction 1 and actual 1 
    True negative: prediction 0 and actual 0
    False positive: prediction 1 and actual 0 Type1
    False negative: prediction 0 and actual 1 Type2
    cm = [[TN,FP], # TN = 0,0 FP = 0,1
          [FN,TP]] # FN = 1,0 TP = 1,1
    
    Accuracy = (TP + TN) / (TP + TN + FP + FN)
    Precision = TP / (TP + FP)
    Recall = TP / (TP + FN)
    F1 Score = 2 * Precision * Recall / (Precision + Recall)
    '''
    (TN,FP),(FN,TP) = cm[0], cm[1]
    accuracy = (TP+TN)/(TP+TN+FP+FN)
    precision = TP/(TP+FP)
    recall = TP/(TP+FN)
    f1score = 2*precision*recall/(precision+recall)
    return {'accuracy':accuracy, 'precision':precision, 
            'recall':recall, 'f1score':f1score}

def get_conf_metrix(test, pred):
    from sklearn.metrics import confusion_matrix
    return confusion_matrix(test, pred)
